# This script demonstrates usage of class CIDEController.
#
# (c) iSystem AG, 2013

from __future__ import print_function

import isystem.connect as ic
import time

cmgr = ic.ConnectionMgr()
cmgr.connectMRU('')

debug = ic.CDebugFacade(cmgr)
exeCtrl = ic.CExecutionController(cmgr)
dataCtrl = ic.CDataController(cmgr)
ide = ic.CIDEController(cmgr)

print('Running winIDEA and testIDEA shutdown script...')

print('Directory of winIDEA executable: ' + ide.getPath(ic.CIDEController.WINIDEA_EXE_DIR))
print('Directory of winIDEA workspace file: ' + ide.getPath(ic.CIDEController.WORKSPACE_DIR))
print('Name of winIDEA workspace file: ' + ide.getPath(ic.CIDEController.WORKSPACE_FILE_NAME))

print("Disconnecting winIDEA from emulator - download to reconnect... ")
ide.iOpenDisconnect()
print("Done.")