#include "Arduino.h"

#define TEMP_OK         10
#define TEMP_TOO_HOT    20
#define TEMP_TOO_COLD   30

byte checkTemperature(long actualTemp, long maxTemp, long minTemp) __attribute__((noinline));

void setup() {
    pinMode(13, OUTPUT);
}

void loop() {
    byte returnVal = 0;
    
    digitalWrite(13, HIGH);
    
    delay(250);
    
    digitalWrite(13, LOW);
    
    delay(350);

    returnVal = checkTemperature(random(-60, 200), random(-60, 200), random(-60, 200));
    
    if (returnVal == TEMP_OK) {
        digitalWrite(13, HIGH);
    
        delay(250);
        
        digitalWrite(13, LOW);
    } else if (returnVal == TEMP_TOO_HOT) {
        digitalWrite(13, HIGH);
    
        delay(500);
        
        digitalWrite(13, LOW);
    } else if (returnVal == TEMP_TOO_COLD) {
        digitalWrite(13, HIGH);
    
        delay(750);
        
        digitalWrite(13, LOW);
    } else {
        digitalWrite(13, HIGH);
    
        delay(2000);
        
        digitalWrite(13, LOW);
    }
    
    delay(500);
}

byte checkTemperature(long actualTemp, long maxTemp, long minTemp) {
    byte returnVal = TEMP_TOO_HOT;
    
    if (actualTemp <= maxTemp) {
        returnVal = TEMP_OK;
    }
    
    if (actualTemp <= minTemp) {
        returnVal = TEMP_TOO_COLD;
    }
    
    return returnVal;    
}