# testIDEA Demo #

This repository is focussed on creating a simple project that can be tested using iSYSTEM's testIDEA software. The main source code file includes a simple function named "checkTemperature" that assesses whether an input variable is lower, higher or between two other values.

There are two important branches:

- master - this contains a working version of the code
- master-2 - this contains a version of the code that has a mistake

The project also includes a Python based script to execute unit tests on the "checkTemperature" function.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Who do I talk to? ###

* Stuart <> Cording at isystem <> com